<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Login model
============================================================== */
class Login_data extends MY_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_data($usr, $pwd, $thn){
        $query = "SELECT u.id_user,
                    u.tahun,
                    u.fid_satker,
                    u.status_satker,
                    u.username,
                    u.user_pass,
                    u.fid_hak,
                    h.hak,
                    h.page,
                    u.user_nama
                    FROM $this->tabel_user u
                    INNER JOIN $this->tabel_hak h ON u.fid_hak = h.id_hak
                    WHERE u.username = ? 
                    AND u.user_pass = ? 
                    AND u.tahun = ?" ;  
        return $this->db_prod->query($query, array($usr, $pwd, $thn));
    } 
    
    public function cek_status($status, $id){
        $query = "select * from $this->tabel_kunci where fid_satker_induk = ?";  
        if(strtoupper($status) == "SUB UNIT"){
            $query = "select * from $this->tabel_kunci where fid_satker = ?";
        }
        return $this->db_prod->query($query, array($id));
    } 
    
    public function get_menu($id_hak){
        $data_menu = array();
        $query = "select * from $this->tabel_menu where fid_hak = ? and parent = 0 and is_active = 1 order by urutan ";
        $result1 = $this->db_prod->query($query, array($id_hak));
        if($result1->num_rows() > 0){
            $data_result1 = $result1->result();
            foreach($data_result1 as $row){
                $submmenu = array();
                $query2 = "select * from $this->tabel_menu where fid_hak = ? and parent = ? and is_active = 1 order by urutan";
                $result2 = $this->db_prod->query($query2, array($id_hak, $row->id_menu));
                if($result2->num_rows() > 0){
                    $data_result2 = $result2->result();
                    foreach($data_result2 as $row_detail){
                        $submmenu[] = array(
                            "id_menu" => $row_detail->id_menu,
                            "nama_menu" => $row_detail->nama_menu,
                            "urutan" => $row_detail->urutan,
                            "link" => $row_detail->link,
                        );
                    }
                }
                $data_menu[] = array(
                    "id_menu" => $row->id_menu,
                    "nama_menu" => $row->nama_menu,
                    "urutan" => $row->urutan,
                    "link" => $row->link,
                    "sub_menu" => $submmenu,
                );
            }
            return $data_menu;
        }else{
            return "";
        }     
    }
    
    public function get_skpd_parent($status, $fid){
        $data = array();
        $data['id_skpd_parent'] = "";
        $data['id_skpd_child'] = "";
        if(strtoupper($status) == "KABKOT"){
            $data['id_skpd_parent'] = "999999";
        }else if (strtoupper($status)=="ADMINISTRATOR" || strtoupper($status)=="EKSEKUTIF" || strtoupper($status)=="SUPERVISOR"){
            $query = 'select * from "'.$this->tabel_6_satker_fix.'" where kd_urusan = 1 and kd_bidang = 6 and kd_unit = 1';
            $result = $this->db_prod->query($query);
            if($result->num_rows() > 0){
                $result_row = $result->row();
                $data['id_skpd_parent'] = $result_row->id_satker_induk;
                
                $query_child = 'SELECT * from "'.$this->tabel_6_satker_fix.'" where id_satker_induk = ? order by nama_satker limit 1';
		$result_child = $this->db_prod->query($query, array($data['id_skpd_parent']));
                if($result_child->num_rows() > 0){
                    $result_row_child = $result_child->row();
                    $data['id_skpd_child'] = $result_row_child->id_satker;
                }
            }else{
                $data['id_skpd_parent'] = 1;
            }
        }else if(strtoupper($status)=="UNIT"){
            $data['id_skpd_parent'] = $fid; 
        }else if(strtoupper($status)=="SUB UNIT"){
            $data['id_skpd_parent'] = $fid; 
        }
        
        return $data;
    }
    
    public function get_skpd_detail($status, $id_parent, $tahun){
        $data_skpd = "";
        if(strtoupper($status) == "KABKOT" || strtoupper($status)=="ADMINISTRATOR" || strtoupper($status)=="EKSEKUTIF" || strtoupper($status)=="SUPERVISOR"){
            $query = "select * from satker_induk where tahun = ? order by nama_satker";
            $result = $this->db_prod->query($query, array($tahun));
            if($result->num_rows() > 0){
                foreach($result->result() as $row){
                    $is_select = 0;
                    if($id_parent == $row->id_satker_induk){
                        $is_select = 1;
                    }
                    $skpd[] = array(
                        "id" => $row->id_satker_induk,
                        "nama" => $row->nama_satker,
                        "tahun" => $row->tahun,
                        "id_bidang" => $row->id_bidang,
                        "kd_urusan" => $row->kd_urusan,
                        "kd_bidang" => $row->kd_bidang,
                        "kd_unit" => $row->kd_unit,
                        "singkatan" => $row->singkatan,
                        "selected" => $is_select,
                    );
                }
                $data_skpd = $skpd;
            }
        }else if(strtoupper($status) == "UNIT"){
            $query = "select * from satker_induk where id_satker_induk  = ?";
            $result = $this->db_prod->query($query, array($id_parent));
            if($result->num_rows() > 0){
                foreach($result->result() as $row){
                    $is_select = 0;
                    if($id_parent == $row->id_satker_induk){
                        $is_select = 1;
                    }
                    $skpd[] = array(
                        "id" => $row->id_satker_induk,
                        "nama" => $row->nama_satker,
                        "tahun" => $row->tahun,
                        "id_bidang" => $row->id_bidang,
                        "kd_urusan" => $row->kd_urusan,
                        "kd_bidang" => $row->kd_bidang,
                        "kd_unit" => $row->kd_unit,
                        "singkatan" => $row->singkatan,
                        "selected" => $is_select,
                    );
                }
                $data_skpd = $skpd;
            }
        }
        return $data_skpd;
    }
}