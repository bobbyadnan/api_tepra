<?php defined('BASEPATH') OR exit('No direct script access allowed');
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Dashboard controller
============================================================== */
class Datadasar extends Site_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
    public function index(){
        // listener controller
        $req = json_decode(file_get_contents('php://input'));
        if(empty($req)){
            echo $this->response_gagal("01", "Wrong parameter");die();
        }
        // call function
        $method = $req->method;// namanya harus sama dengan function yang dituju
        $response = $this->$method($req);
        echo $response;
    }
    
    protected function data_pimpinan($param){
        $data = array();
        $this->load->model('datadasar/pimpinan');
        
        // parameter yang dibutuhkan
        $tahun = $param->user->user_tahun;
        $id_parent = $param->user->user_id_skpd_parent;
        $id_child = $param->user->user_id_skpd_child;
        
        $result = $this->pimpinan->get_data($tahun, $id_parent, $id_child);
        if($result->num_rows() > 0){
            foreach($result->result() as $row){
                $data[] = array(
                    "id_pimpinan" => $row->id_pimpinan,
                    "nm_pimpinan" => $row->nama_pimpinan,
                    "jabatan" => $row->jabatan_pimpinan,
                    "alamat" => $row->alamat_pimpinan,
                    "hp" => $row->hp_pimpinan,
                    "email" => $row->email_pimpinan,
                    "nip" => $row->nip_pimpinan,
                    "singkatan_induk" => $row->singkatan,
                );
            }
            echo $this->response_sukses($data);
        }else{
            echo $this->response_gagal("01", "Data Not availalbe");
        }
    }
}