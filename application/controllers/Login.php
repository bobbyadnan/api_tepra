<?php defined('BASEPATH') OR exit('No direct script access allowed');
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Login controller
============================================================== */
class Login extends Site_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
    public function index(){
        $data = array();
        $req = json_decode(file_get_contents('php://input'));
        if(empty($req)){
            echo $this->response_gagal("01", "Wrong parameter");die();
        }
        
        $this->load->model("login_data");
        $result = $this->login_data->get_data($req->username, $req->password, $req->tahun);
        if($result->num_rows() > 0){
            $result_data = $result->row();
            $data["user_pemda"] = "KOTA BALIKPAPAN";
            $data["user_tahun"]	 = $req->tahun;
            $data["user_id"] = $result_data->id_user;
            $data["user_id_hak"] = $result_data->fid_hak;
            $data["user_status"] = $result_data->hak;
            $data["user_name"] = $result_data->user_nama;
            $data["user_fid"] = $result_data->fid_satker;
            // checking status
            $status = $this->login_data->cek_status($data["user_status"], $data["user_fid"]);
            if($status->num_rows() > 0){
                // jika available, user di lock
                echo $this->response_gagal("02", "Username locked");die();
            }else{
                // ambil data menu
                $data["user_menu"] = $this->login_data->get_menu($data["user_id_hak"]);
                
                // init data skppd
                $data["user_id_skpd"] = $this->login_data->get_skpd_parent($data["user_status"], $data["user_fid"]);
                
                // detail skpd
                $data["user_detail_skpd"] = $this->login_data->get_skpd_detail($data["user_status"], $data["user_id_skpd"]['id_skpd_parent'], $data["user_tahun"]);
            }
            echo $this->response_sukses($data);
        }else{
            echo $this->response_gagal("02", "Username atau Password tidak cocok");die();
        }
    }
}