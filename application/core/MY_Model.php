<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class 
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Code Model
============================================================== */
MY_Model extends CI_Model {

    protected $ci;
    public $db_prod;
    public $db_devel;
    
    // define table name
    public $tabel_user = "_user_fix";
    public $tabel_hak = "_hak";
    public $tabel_kunci = "kunci";
    public $tabel_menu = "menu";
    public $tabel_6_satker_fix = "6_satker_fix";
    
    function __construct()
    {
        parent::__construct();
        $this->ci=&get_instance();
        $this->db_prod = $this->load->database('default', TRUE);
        $this->db_devel = $this->load->database('devel', TRUE);
    }
    
}
?>