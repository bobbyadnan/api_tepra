<?php
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Site_Controller as CI Controller
============================================================== */
class Site_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
     
    public function response_sukses($param){
        $data = array(
            "response_code" => "00",
            "response_desc" => "SUCCESS",
            "response_value" => $param,
        );
        return json_encode($data);
    }
    
    public function response_gagal($rc, $ket){
        $data = array(
            "response_code" => $rc,
            "response_desc" => $ket,
            "response_value" => NULL,
        );
        return json_encode($data);
    }
}